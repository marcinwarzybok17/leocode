export default {
    clearMocks: true,
    collectCoverage: true,
    coverageDirectory: 'coverage',
    setupFiles: ['./test/config/jest.setup.ts'],
};
