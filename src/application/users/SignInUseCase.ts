import { Result, RESULT_STATUS } from '../../domain/common/Result';
import { UsersRepository } from '../../domain/users/UsersRepository';
import { TokenBaseAuthorization } from '../../infrastructure/repositories/TokenBaseAuthorization';
import { UseCase } from '../common/UseCase';

export type SignInUseCaseType = Promise<Result<RESULT_STATUS, string>>;
export class SignInUseCase implements UseCase<SignInUseCaseType> {
    constructor(
        private usersRepository: UsersRepository,
        private tokenBaseAuthentication: TokenBaseAuthorization
    ) {
        this.usersRepository = usersRepository;
        this.tokenBaseAuthentication = tokenBaseAuthentication;
    }

    async execute(email: string, password: string) {
        const result = this.usersRepository.getUserByEmail(email);

        if (result.resultStatus === RESULT_STATUS.FAILURE) {
            return result;
        }

        // in future bcrypt or something other type of hash/decode method of storing can be use
        if (result.value.password !== password) {
            return Result.fail(401, 'Invalid password');
        }

        const token = await this.tokenBaseAuthentication.encode({
            email,
        });

        return Result.ok(200, token);
    }
}
