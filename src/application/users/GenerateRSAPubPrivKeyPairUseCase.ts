import { Result, RESULT_STATUS } from '../../domain/common/Result';
import { UsersRepository } from '../../domain/users/UsersRepository';
import { RSAPubPrivKeyPairRepository } from '../../infrastructure/repositories/RSAPubPrivKeyPairRepository';
import { UseCase } from '../common/UseCase';

export type GenerateRSAPubPrivKeyPairUseCaseType = Promise<
    | Result<RESULT_STATUS.FAILURE, string>
    | Result<
          RESULT_STATUS.SUCCESS,
          {
              publicKey: string;
              privateKey: string;
          }
      >
>;
export class GenerateRSAPubPrivKeyPairUseCase
    implements UseCase<GenerateRSAPubPrivKeyPairUseCaseType>
{
    constructor(private usersRepository: UsersRepository) {
        this.usersRepository = usersRepository;
    }

    async execute(email: string) {
        const result = this.usersRepository.getUserByEmail(email);

        if (result.resultStatus === RESULT_STATUS.FAILURE) {
            return result;
        }

        const { publicKey, privateKey } = result.value;

        if (publicKey && privateKey) {
            return Result.ok(200, {
                publicKey,
                privateKey,
            });
        }

        const keyPair = await RSAPubPrivKeyPairRepository.generate();
        const generatedPrivateKey = keyPair.privateKey.toString();
        const generatedPublicKey = keyPair.publicKey.toString();

        this.usersRepository.updateRSAKeyPairByEmail(
            email,
            generatedPrivateKey,
            generatedPublicKey
        );

        return Result.ok(200, {
            publicKey: generatedPublicKey,
            privateKey: generatedPrivateKey,
        });
    }
}
