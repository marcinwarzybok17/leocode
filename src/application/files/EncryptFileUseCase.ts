import { Result, RESULT_STATUS } from '../../domain/common/Result';
import { EncryptionFileRepository } from '../../domain/files/EncryptionFileRepository';
import { UsersRepository } from '../../domain/users/UsersRepository';
import { UseCase } from '../common/UseCase';

export type EncryptFileUseCaseReturnType = Promise<
    Result<RESULT_STATUS, string>
>;

export class EncryptFileUseCase
    implements UseCase<EncryptFileUseCaseReturnType>
{
    constructor(
        private encryptionFileRepository: EncryptionFileRepository,
        private usersRepository: UsersRepository
    ) {}

    async execute(email: string, fileBuffer: Buffer) {
        const getUserResult = this.usersRepository.getUserByEmail(email);

        if (getUserResult.resultStatus === RESULT_STATUS.FAILURE) {
            return getUserResult;
        }

        const { publicKey } = getUserResult.value;

        const encryptedFileInString =
            await this.encryptionFileRepository.encrypt(fileBuffer, publicKey);

        return Result.ok(200, encryptedFileInString);
    }
}
