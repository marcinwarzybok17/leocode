export interface UseCase<T> {
    execute(...args): T;
}
