import { Main } from './Main';

(async () => {
    const main = new Main();
    await main.start();
})();
