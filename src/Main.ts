import { ConnectionManager } from './infrastructure/servers/ConnectionManager';
import { ServerConnectionManager } from './infrastructure/servers/ServerConnectionManager';
import 'reflect-metadata';
import { DatabaseDataLoader } from './infrastructure/databases/DatabaseDataLoader';
import { InMemoryUsersDatabase } from './infrastructure/repositories/InMemoryUsersDatabase';
import { UsersDatabaseDataLoader } from './infrastructure/databases/UsersDatabaseDataLoader';

export interface IMain {
    start(): Promise<void>;
    stop(): Promise<void>;
}

export class Main implements IMain {
    serverConnectionManager: ConnectionManager;
    usersDatabaseDataLoader: DatabaseDataLoader;

    async start(): Promise<void> {
        this.usersDatabaseDataLoader = new UsersDatabaseDataLoader(
            InMemoryUsersDatabase.getInstance()
        );
        this.usersDatabaseDataLoader.loadDataToDatabase();
        this.serverConnectionManager =
            await ServerConnectionManager.createConnections();
    }

    async stop(): Promise<void> {
        if (!this.serverConnectionManager) {
            throw new Error(
                "Server connections can't be close due missing initialization"
            );
        }
        await this.serverConnectionManager.stopAll();
    }
}
