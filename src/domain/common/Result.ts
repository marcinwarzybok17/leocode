export enum RESULT_STATUS {
    SUCCESS = 'success',
    FAILURE = 'failure',
}

export class Result<RESULT_STATUS, T> {
    public resultStatus: RESULT_STATUS;
    public resultCode: number;
    public value: T;

    private constructor(
        resultStatus: RESULT_STATUS,
        resultCode: number,
        value?: T
    ) {
        this.resultStatus = resultStatus;
        this.resultCode = resultCode;
        this.value = value;
    }

    static ok<U>(
        resultCode: number,
        value?: U
    ): Result<RESULT_STATUS.SUCCESS, U> {
        return new Result<RESULT_STATUS.SUCCESS, U>(
            RESULT_STATUS.SUCCESS,
            resultCode,
            value
        );
    }

    public static fail(
        resultCode: number,
        error: string
    ): Result<RESULT_STATUS.FAILURE, string> {
        return new Result<RESULT_STATUS.FAILURE, string>(
            RESULT_STATUS.FAILURE,
            resultCode,
            error
        );
    }
}
