export interface EncryptionFileRepository {
    encrypt(buffer: Buffer, key: string): string | Promise<string>;
}
