import { Result, RESULT_STATUS } from '../common/Result';
import { User } from './User';

export type UserOrErrorType =
    | Result<RESULT_STATUS.SUCCESS, User>
    | Result<RESULT_STATUS.FAILURE, string>;

export interface UsersRepository {
    save(user: User): UserOrErrorType;
    getUserByEmail(email: string): UserOrErrorType;
    updateRSAKeyPairByEmail(
        email: string,
        publicKey: string,
        privateKey: string
    ): Result<RESULT_STATUS.SUCCESS, {}> | Result<RESULT_STATUS.FAILURE, {}>;
}
