import { IsEmail, IsString, Validate } from 'class-validator';
import { UserComplexPasswordPolicy } from './UserComplexPasswordPolicy';

export class User {
    @IsEmail()
    email: string;

    @IsString()
    @Validate(UserComplexPasswordPolicy)
    password: string;

    publicKey?: string;
    privateKey?: string;
}
