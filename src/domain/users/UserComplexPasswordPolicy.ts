import { ValidatorConstraint } from 'class-validator';
import RE2 from 're2';
import { PasswordPolicy } from './PasswordPolicy';

@ValidatorConstraint()
export class UserComplexPasswordPolicy extends PasswordPolicy {
    protected passwordPolicyRegex: RE2 = new RE2(
        '^(?=.*[a-z])(?=.*[A-Z])(?=.*d)(?=.*[@$!%*?&])[A-Za-zd@$!%*?&]{8,30}$'
    );

    defaultMessage(): string {
        return 'Minimum 8 and maximum 30 characters, at least one uppercase letter, one lowercase letter, one number and one special character';
    }
}
