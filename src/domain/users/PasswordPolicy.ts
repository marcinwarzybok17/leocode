import {
    ValidationArguments,
    ValidatorConstraintInterface,
} from 'class-validator';
import RE2 from 're2';

export abstract class PasswordPolicy implements ValidatorConstraintInterface {
    protected passwordPolicyRegex: RE2;
    validate(passwordToValidate: string): boolean | Promise<boolean> {
        return this.passwordPolicyRegex.test(passwordToValidate);
    }
    abstract defaultMessage(validationArguments?: ValidationArguments): string;
}
