export default {
    apiPort: Number(process.env.API_PORT),
    user1: {
        email: process.env.USER_ONE_EMAIL,
        password: process.env.USER_ONE_PASSWORD,
    },
    user2: {
        email: process.env.USER_TWO_EMAIL,
        password: process.env.USER_TWO_PASSWORD,
    },
    jwtKey: process.env.JWT_PRIVATE_KEY,
};
