export interface DatabaseDataLoader {
    loadDataToDatabase(): void;
}
