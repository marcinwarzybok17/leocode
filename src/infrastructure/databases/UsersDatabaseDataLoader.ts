import { UsersRepository } from '../../domain/users/UsersRepository';
import { DatabaseDataLoader } from './DatabaseDataLoader';
import config from '../config/config';

export class UsersDatabaseDataLoader implements DatabaseDataLoader {
    constructor(private usersRepository: UsersRepository) {}

    loadDataToDatabase(): void {
        const { user1, user2 } = config;
        const users = [user1, user2];

        users.forEach((user) => {
            this.usersRepository.save(user);
        });
    }
}
