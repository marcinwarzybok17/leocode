import { Server } from './Server';
import express from 'express';
import { Server as HttpServer } from 'http';
import { promisify } from 'util';
import { ExpressRouteRegisterManager } from './ExpressRouteRegisterManager';
import { Middleware } from './controllers/Middleware';

export abstract class ExpressServer implements Server {
    private serverInstance: HttpServer;
    protected expressRouteRegisterManager: ExpressRouteRegisterManager;
    protected expressMiddlewareErrorHandler: Middleware;

    async start(port: number): Promise<void> {
        const app = express();
        app.use(express.urlencoded({ extended: true }));
        app.use(express.json());

        this.expressRouteRegisterManager.registerControllers(app);

        app.use(
            this.expressMiddlewareErrorHandler.execute.bind(
                this.expressMiddlewareErrorHandler
            )
        );

        this.serverInstance = app.listen(port, () => {
            console.log(`Server started on port ${port}`);
        });
    }

    async stop(): Promise<void> {
        if (!this.serverInstance) {
            throw new Error(`Can't stop server that is not running`);
        }

        const close = promisify(this.serverInstance.close).bind(
            this.serverInstance
        );
        await close();
    }
}
