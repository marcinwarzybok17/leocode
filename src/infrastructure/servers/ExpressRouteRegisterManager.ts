import { Route } from './routes/Route';
import { Application } from 'express';

export interface ExpressRouteRegisterManager {
    routes: Route[];
    registerControllers(app: Application): void;
}
