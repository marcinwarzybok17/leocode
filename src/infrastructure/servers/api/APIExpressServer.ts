import { RouteRegisterManagerByHttpMethod } from './../RouteRegisterManagerByHttpMethod';
import { ExpressServer } from './../ExpressServer';
import { SignInUseCase } from '../../../application/users/SignInUseCase';
import { InMemoryUsersDatabase } from '../../repositories/InMemoryUsersDatabase';
import { JWTTokenRepository } from '../../repositories/JWTTokenRepository';
import { GenerateRSAPubPrivKeyPairUseCase } from '../../../application/users/GenerateRSAPubPrivKeyPairUseCase';
import { EncryptFileUseCase } from '../../../application/files/EncryptFileUseCase';
import { Base64EncryptFileRepository } from '../../repositories/Base64EncryptFileRepository';
import { UserAuthenticatedMiddleware } from '../controllers/middlewares/UserAuthenticatedMiddleware';
import { FileUploadController } from '../controllers/files/FileUploadController';
import { SingleFileUploadMiddleware } from '../controllers/middlewares/SingleFileUploadMiddleware';
import { ExpressDefaultErrorHandler } from '../controllers/users/ExpressDefaultErrorHandler';
import { GenerateKeyPairController } from '../controllers/users/GenerateKeyPairController';
import { SignInController } from '../controllers/users/SignInController';
import { Route, ROUTE_METHOD } from '../routes/Route';

export class APIExpressServer extends ExpressServer {
    constructor() {
        super();
        const inMemoryUsersDatabase = InMemoryUsersDatabase.getInstance();
        const jwtTokenRepository = new JWTTokenRepository();
        const userAuthenticatedMiddleware = new UserAuthenticatedMiddleware(
            jwtTokenRepository
        );
        const routes: Route[] = [
            {
                controllers: [
                    new SignInController(
                        new SignInUseCase(
                            inMemoryUsersDatabase,
                            jwtTokenRepository
                        )
                    ),
                ],
                method: ROUTE_METHOD.POST,
                path: '/api/sign-in',
            },
            {
                controllers: [
                    userAuthenticatedMiddleware,
                    new GenerateKeyPairController(
                        new GenerateRSAPubPrivKeyPairUseCase(
                            inMemoryUsersDatabase
                        )
                    ),
                ],
                method: ROUTE_METHOD.POST,
                path: '/api/generate-key-pair',
            },
            {
                controllers: [
                    userAuthenticatedMiddleware,
                    new SingleFileUploadMiddleware(),
                    new FileUploadController(
                        new EncryptFileUseCase(
                            new Base64EncryptFileRepository(),
                            inMemoryUsersDatabase
                        )
                    ),
                ],
                method: ROUTE_METHOD.POST,
                path: '/api/encrypt',
            },
        ];
        const expressRouteRegisterManager =
            new RouteRegisterManagerByHttpMethod(routes);
        this.expressRouteRegisterManager = expressRouteRegisterManager;
        this.expressMiddlewareErrorHandler = new ExpressDefaultErrorHandler();
    }
}
