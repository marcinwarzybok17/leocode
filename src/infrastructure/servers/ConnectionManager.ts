export interface ConnectionManagerStaticMethods {
    createConnections(): unknown;
}

export interface ConnectionManager {
    stopAll(): unknown;
}
