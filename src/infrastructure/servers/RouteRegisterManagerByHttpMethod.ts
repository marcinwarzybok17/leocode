import { ExpressRouteRegisterManager } from './ExpressRouteRegisterManager';
import { Application } from 'express';
import { Route, ROUTE_METHOD } from './routes/Route';

export class RouteRegisterManagerByHttpMethod
    implements ExpressRouteRegisterManager
{
    routes: Route[];

    constructor(routes: Route[]) {
        this.routes = routes;
    }

    registerControllers(app: Application): void {
        this.routes.forEach(({ controllers, method, path }) => {
            const middlewaresAndControllers = controllers.map((controller) => {
                return controller.execute.bind(controller);
            });
            if (method === ROUTE_METHOD.POST) {
                app.post(path, ...middlewaresAndControllers);
            }
        });
    }
}
