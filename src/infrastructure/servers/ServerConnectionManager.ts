import { allowStaticMethods } from '../../utils/allowStaticMethod';
import config from '../config/config';
import { APIExpressServer } from './api/APIExpressServer';
import {
    ConnectionManager,
    ConnectionManagerStaticMethods,
} from './ConnectionManager';
import { Server } from './Server';
import { SERVERS } from './SERVERS';

@allowStaticMethods<ConnectionManagerStaticMethods>()
export class ServerConnectionManager implements ConnectionManager {
    serverConnections: Map<string, Server> = new Map();
    private static instance: ServerConnectionManager;

    static async createConnections(): Promise<ConnectionManager> {
        let serverConnectionManager = ServerConnectionManager.instance;

        if (!serverConnectionManager) {
            serverConnectionManager = new ServerConnectionManager();
        }

        serverConnectionManager.serverConnections.set(
            SERVERS.API,
            new APIExpressServer()
        );

        await serverConnectionManager.startAll();

        return serverConnectionManager;
    }

    async startAll(): Promise<void> {
        await this.serverConnections.get(SERVERS.API).start(config.apiPort);
    }

    async stopAll(): Promise<void> {
        await this.serverConnections.get(SERVERS.API).stop();
    }
}
