import { AuthenticatedRequest } from '../AuthenticatedRequest';
import { Controller } from '../Controller';
import { NextFunction, Response } from 'express';
import { UseCase } from '../../../../application/common/UseCase';
import { EncryptFileUseCaseReturnType } from '../../../../application/files/EncryptFileUseCase';
import { RESULT_STATUS } from '../../../../domain/common/Result';

export class FileUploadController implements Controller {
    constructor(
        private encryptFileUseCase: UseCase<EncryptFileUseCaseReturnType>
    ) {}

    async execute(
        req: AuthenticatedRequest,
        res: Response,
        next: NextFunction
    ) {
        const { email } = req.user;
        const { file } = req;
        const result = await this.encryptFileUseCase.execute(
            email,
            file.buffer
        );

        if (result.resultStatus === RESULT_STATUS.SUCCESS) {
            res.status(result.resultCode).send({
                encrypted: result.value,
            });
            return;
        }

        next(result);
    }
}
