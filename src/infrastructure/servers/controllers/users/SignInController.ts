import { NextFunction, Request, Response } from 'express';
import { UseCase } from '../../../../application/common/UseCase';
import { SignInUseCaseType } from '../../../../application/users/SignInUseCase';
import { RESULT_STATUS } from '../../../../domain/common/Result';
import { Controller } from '../Controller';

export class SignInController implements Controller {
    private signInUseCase: UseCase<SignInUseCaseType>;

    constructor(signInUseCase: UseCase<SignInUseCaseType>) {
        this.signInUseCase = signInUseCase;
    }

    async execute(
        req: Request,
        res: Response,
        next: NextFunction
    ): Promise<void> {
        const { email, password } = req.body;
        const result = await this.signInUseCase.execute(email, password);

        if (result.resultStatus === RESULT_STATUS.SUCCESS) {
            res.status(result.resultCode).send({
                authToken: result.value,
            });
            return;
        }

        next(result);
    }
}
