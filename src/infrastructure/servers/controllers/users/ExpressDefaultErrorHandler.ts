import { Middleware } from '../Middleware';
import { Request, Response, NextFunction } from 'express';
import { Result, RESULT_STATUS } from '../../../../domain/common/Result';

export class ExpressDefaultErrorHandler implements Middleware {
    execute(
        err: Result<RESULT_STATUS.FAILURE, any>,
        _req: Request,
        res: Response,
        next: NextFunction
    ): void {
        if (typeof err.value === 'string') {
            res.status(err.resultCode).send({ message: err.value });
            return;
        }

        if (typeof err.value === 'object') {
            res.status(err.resultCode).send({ ...err.value });
            return;
        }

        next(err);
    }
}
