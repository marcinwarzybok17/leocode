import { NextFunction, Response } from 'express';
import { UseCase } from '../../../../application/common/UseCase';
import { GenerateRSAPubPrivKeyPairUseCaseType } from '../../../../application/users/GenerateRSAPubPrivKeyPairUseCase';
import { RESULT_STATUS } from '../../../../domain/common/Result';
import { AuthenticatedRequest } from '../AuthenticatedRequest';
import { Controller } from '../Controller';

export class GenerateKeyPairController implements Controller {
    private generateRSAPubPrivKeyPairUseCase: UseCase<GenerateRSAPubPrivKeyPairUseCaseType>;

    constructor(
        generateRSAPubPrivKeyPairUseCase: UseCase<GenerateRSAPubPrivKeyPairUseCaseType>
    ) {
        this.generateRSAPubPrivKeyPairUseCase =
            generateRSAPubPrivKeyPairUseCase;
    }

    async execute(
        req: AuthenticatedRequest,
        res: Response,
        next: NextFunction
    ): Promise<void> {
        const { email } = req.user;
        const getUserResult =
            await this.generateRSAPubPrivKeyPairUseCase.execute(email);

        if (getUserResult.resultStatus === RESULT_STATUS.SUCCESS) {
            res.status(getUserResult.resultCode).send(getUserResult.value);
            return;
        }

        next(getUserResult);
    }
}
