import { NextFunction, Response } from 'express';
import { TokenBaseAuthorization } from '../../../repositories/TokenBaseAuthorization';
import { AuthenticatedRequest } from '../AuthenticatedRequest';
import { Controller } from '../Controller';

export class UserAuthenticatedMiddleware implements Controller {
    constructor(private tokenBaseAuthorization: TokenBaseAuthorization) {}

    async execute(
        req: AuthenticatedRequest,
        res: Response,
        next: NextFunction
    ) {
        const { authorization } = req.headers;

        if (!authorization) {
            res.status(401).send({});
            return;
        }

        try {
            const { email } = await this.tokenBaseAuthorization.decode(
                authorization
            );
            req.user = {
                email,
            };
        } catch (err) {
            res.status(401).send({ message: 'Token expire or is invalid' });
        }

        next();
    }
}
