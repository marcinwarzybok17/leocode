import { Controller } from '../Controller';
import { NextFunction, Response } from 'express';
import { AuthenticatedRequest } from '../AuthenticatedRequest';
import multer from 'multer';

const upload = multer({ storage: multer.memoryStorage() });
const uploadSingleFile = upload.single('file');

export class SingleFileUploadMiddleware implements Controller {
    async execute(
        req: AuthenticatedRequest,
        res: Response,
        next: NextFunction
    ) {
        uploadSingleFile(req, res, next);
    }
}
