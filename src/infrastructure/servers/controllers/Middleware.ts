import { Request, Response, NextFunction } from 'express';

export interface Middleware {
    execute(err: any, req: Request, res: Response, next: NextFunction): unknown;
}
