import { Controller } from '../controllers/Controller';

export enum ROUTE_METHOD {
    POST,
}

export interface Route {
    path: string;
    method: ROUTE_METHOD;
    controllers: Controller[];
}
