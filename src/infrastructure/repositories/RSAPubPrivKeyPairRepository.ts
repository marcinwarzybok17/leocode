import {
    generateKeyPair as generateKeyPairCrypto,
    KeyExportOptions,
} from 'crypto';
import { promisify } from 'util';

const generateKeyPair = promisify(generateKeyPairCrypto);
const exportOptions: KeyExportOptions<'pem'> = {
    format: 'pem',
    type: 'pkcs1',
};

export class RSAPubPrivKeyPairRepository {
    static async generate() {
        const { privateKey, publicKey } = await generateKeyPair('rsa', {
            modulusLength: 2048,
        });

        return {
            privateKey: privateKey.export(exportOptions),
            publicKey: publicKey.export(exportOptions),
        };
    }
}
