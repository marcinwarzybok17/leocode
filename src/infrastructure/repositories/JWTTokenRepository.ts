import { TokenBaseAuthorization } from './TokenBaseAuthorization';
import jwt, { JwtPayload, Secret, SignOptions } from 'jsonwebtoken';
import config from '../config/config';
import { promisify } from 'util';

const sign = promisify<object, Secret, SignOptions, string>(jwt.sign);
const verify = promisify<string, Secret, JwtPayload | string>(jwt.verify);

export class JWTTokenRepository implements TokenBaseAuthorization {
    async decode(token: string) {
        const decoded = await verify(token, config.jwtKey);
        return decoded as { email: string };
    }

    async encode(payload: object): Promise<string> {
        return await sign(payload, config.jwtKey, {
            algorithm: 'HS256',
            expiresIn: 300,
        });
    }
}
