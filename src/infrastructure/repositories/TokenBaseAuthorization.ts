export interface TokenBaseAuthorization {
    encode(payload: object): string | Promise<string>;
    decode(token: string): { email: string } | Promise<{ email: string }>;
}
