import {
    UserOrErrorType,
    UsersRepository,
} from '../../domain/users/UsersRepository';
import { User } from '../../domain/users/User';
import { Result, RESULT_STATUS } from '../../domain/common/Result';

export class InMemoryUsersDatabase implements UsersRepository {
    public static instance: InMemoryUsersDatabase;
    private store: Map<string, User> = new Map();

    static getInstance(): InMemoryUsersDatabase {
        if (InMemoryUsersDatabase.instance) {
            return InMemoryUsersDatabase.instance;
        }

        const inMemoryUsersDatabase = new InMemoryUsersDatabase();
        InMemoryUsersDatabase.instance = inMemoryUsersDatabase;

        return inMemoryUsersDatabase;
    }

    save(user: User): UserOrErrorType {
        const { email } = user;

        if (this.store.has(email)) {
            return Result.fail(400, 'Email already exists');
        }

        this.store.set(email, user);
        return Result.ok(200, user);
    }

    getUserByEmail(email: string): UserOrErrorType {
        const user = this.store.get(email);

        if (!user) {
            return Result.fail(401, "User with this email doesn't exist");
        }

        return Result.ok(200, user);
    }

    updateRSAKeyPairByEmail(
        email: string,
        publicKey: string,
        privateKey: string
    ): Result<RESULT_STATUS.SUCCESS, {}> | Result<RESULT_STATUS.FAILURE, {}> {
        const user = this.store.get(email);

        if (!user) {
            return Result.fail(401, "User with this email doesn't exist");
        }

        this.store.set(email, {
            ...user,
            publicKey,
            privateKey,
        });

        return Result.ok(200, user);
    }
}
