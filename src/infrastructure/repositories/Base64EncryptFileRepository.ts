import { EncryptionFileRepository } from '../../domain/files/EncryptionFileRepository';
import { createCipheriv, randomFill, scrypt } from 'crypto';

export class Base64EncryptFileRepository implements EncryptionFileRepository {
    encrypt(fileInBuffer: Buffer, publicKey: string): Promise<string> {
        return new Promise((resolve, reject) => {
            scrypt(publicKey, 'salt', 24, (err, key) => {
                if (err) throw err;

                randomFill(new Uint8Array(16), (err, iv) => {
                    if (err) throw err;

                    const cipher = createCipheriv('aes-192-cbc', key, iv);

                    let encrypted = '';
                    cipher.setEncoding('hex');

                    cipher.on('data', (chunk) => (encrypted += chunk));
                    cipher.on('error', (err) => reject(err));
                    cipher.once('end', () => {
                        resolve(encrypted);
                    });

                    cipher.write(fileInBuffer);
                    cipher.end();
                });
            });
        });
    }
}
