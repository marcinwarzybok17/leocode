# Setup and starting instructions

Open terminal in root folder:

1. `cp .env.example .env` and fill environment variables with your custom values or leave it as it is.
2. `docker-compose up` to start a whole environment
3. User emails and passwords are saved in .env.example
4. You can test via Postman, URL-s are the same as in task

# What can be make better

-   Cover project with tests
-   DTO classes should be added between controllers and application layer
-   i should use AES encryption but don't have time to read about cryptography, file buffers are too large for simple public encryption
-   use some hashing password library/method before save password

# What approaches i use ?

-   I'm studying Domain Driven Design now and Domain Model approach, so i strive to create code based on this approach
-   Maintain as much SOLID principles as possible

**Time spent**: 14h
